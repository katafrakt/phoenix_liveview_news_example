defmodule Crowdnews.NewsTest do
  use Crowdnews.DataCase

  alias Crowdnews.News

  describe "news_posts" do
    alias Crowdnews.News.Post

    @valid_attrs %{summary: "some summary", title: "some title"}
    @update_attrs %{summary: "some updated summary", title: "some updated title"}
    @invalid_attrs %{summary: nil, title: nil}

    def post_fixture(attrs \\ %{}) do
      {:ok, post} =
        attrs
        |> Enum.into(@valid_attrs)
        |> News.create_post()

      post
    end

    test "list_news_posts/0 returns all news_posts" do
      post = post_fixture()
      assert News.list_news_posts() == [post]
    end

    test "get_post!/1 returns the post with given id" do
      post = post_fixture()
      assert News.get_post!(post.id) == post
    end

    test "create_post/1 with valid data creates a post" do
      assert {:ok, %Post{} = post} = News.create_post(@valid_attrs)
      assert post.summary == "some summary"
      assert post.title == "some title"
    end

    test "create_post/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = News.create_post(@invalid_attrs)
    end

    test "update_post/2 with valid data updates the post" do
      post = post_fixture()
      assert {:ok, %Post{} = post} = News.update_post(post, @update_attrs)
      assert post.summary == "some updated summary"
      assert post.title == "some updated title"
    end

    test "update_post/2 with invalid data returns error changeset" do
      post = post_fixture()
      assert {:error, %Ecto.Changeset{}} = News.update_post(post, @invalid_attrs)
      assert post == News.get_post!(post.id)
    end

    test "delete_post/1 deletes the post" do
      post = post_fixture()
      assert {:ok, %Post{}} = News.delete_post(post)
      assert_raise Ecto.NoResultsError, fn -> News.get_post!(post.id) end
    end

    test "change_post/1 returns a post changeset" do
      post = post_fixture()
      assert %Ecto.Changeset{} = News.change_post(post)
    end
  end

  describe "news_updates" do
    alias Crowdnews.News.Update

    @valid_attrs %{author: "some author", content: "some content", news_id: 42}
    @update_attrs %{author: "some updated author", content: "some updated content", news_id: 43}
    @invalid_attrs %{author: nil, content: nil, news_id: nil}

    def update_fixture(attrs \\ %{}) do
      {:ok, update} =
        attrs
        |> Enum.into(@valid_attrs)
        |> News.create_update()

      update
    end

    test "list_news_updates/0 returns all news_updates" do
      update = update_fixture()
      assert News.list_news_updates() == [update]
    end

    test "get_update!/1 returns the update with given id" do
      update = update_fixture()
      assert News.get_update!(update.id) == update
    end

    test "create_update/1 with valid data creates a update" do
      assert {:ok, %Update{} = update} = News.create_update(@valid_attrs)
      assert update.author == "some author"
      assert update.content == "some content"
      assert update.news_id == 42
    end

    test "create_update/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = News.create_update(@invalid_attrs)
    end

    test "update_update/2 with valid data updates the update" do
      update = update_fixture()
      assert {:ok, %Update{} = update} = News.update_update(update, @update_attrs)
      assert update.author == "some updated author"
      assert update.content == "some updated content"
      assert update.news_id == 43
    end

    test "update_update/2 with invalid data returns error changeset" do
      update = update_fixture()
      assert {:error, %Ecto.Changeset{}} = News.update_update(update, @invalid_attrs)
      assert update == News.get_update!(update.id)
    end

    test "delete_update/1 deletes the update" do
      update = update_fixture()
      assert {:ok, %Update{}} = News.delete_update(update)
      assert_raise Ecto.NoResultsError, fn -> News.get_update!(update.id) end
    end

    test "change_update/1 returns a update changeset" do
      update = update_fixture()
      assert %Ecto.Changeset{} = News.change_update(update)
    end
  end
end
