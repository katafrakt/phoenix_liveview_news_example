defmodule Crowdnews.News.Post do
  use Ecto.Schema
  import Ecto.Changeset

  schema "news_posts" do
    field :summary, :string
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(post, attrs) do
    post
    |> cast(attrs, [:title, :summary])
    |> validate_required([:title, :summary])
  end
end
