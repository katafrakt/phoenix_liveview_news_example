defmodule Crowdnews.News.Update do
  use Ecto.Schema
  import Ecto.Changeset

  schema "news_updates" do
    field :author, :string
    field :content, :string
    field :news_id, :integer

    timestamps()
  end

  @doc false
  def changeset(update, attrs) do
    update
    |> cast(attrs, [:news_id, :author, :content])
    |> validate_required([:news_id, :author, :content])
  end
end
