defmodule Crowdnews.Repo do
  use Ecto.Repo,
    otp_app: :crowdnews,
    adapter: Ecto.Adapters.Postgres
end
