defmodule Crowdnews.NewsLiveView do
  use Phoenix.LiveView
  alias Crowdnews.News
  alias Crowdnews.PubSub
  alias CrowdnewsWeb.PostView

  def mount(session, socket) do
    IO.puts("Subscribing to #{"update:#{session.post.id}"}")

    if connected?(socket), do: Phoenix.PubSub.subscribe(PubSub, "update:#{session.post.id}")

    {:ok, assign(socket, session)}
  end

  def render(assigns) do
    PostView.render("show.html", assigns)
  end

  def handle_info({:new_update, post}, socket) do
    IO.puts "Handling"
    updates = News.get_updates_for_post(post)
    {:noreply, assign(socket, updates: updates)}
  end

  def handle_event("add_update", params, socket) do
    News.create_update(params["update"])
    {:noreply, socket}
  end
end