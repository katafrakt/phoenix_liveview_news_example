defmodule CrowdnewsWeb.Router do
  use CrowdnewsWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug Phoenix.LiveView.Flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CrowdnewsWeb do
    pipe_through :browser

    get "/", PostController, :index
    resources "/news_posts", PostController, only: [:new, :create, :show]
    resources "/updates", UpdateController, only: [:create]
  end

  # Other scopes may use custom stacks.
  # scope "/api", CrowdnewsWeb do
  #   pipe_through :api
  # end
end
