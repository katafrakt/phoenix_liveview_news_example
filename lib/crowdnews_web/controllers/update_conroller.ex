defmodule CrowdnewsWeb.UpdateController do
  use CrowdnewsWeb, :controller

  alias Crowdnews.News
  alias Crowdnews.News.Post
  alias Crowdnews.News.Update

  def create(conn, %{"update" => update_params}) do
    case News.create_update(update_params) do
      {:ok, update} ->
        conn
        |> put_flash(:info, "Update created successfully.")
        |> redirect(to: Routes.post_path(conn, :show, update.news_id))

      {:error, %Ecto.Changeset{} = changeset} ->
        conn
        |> put_flash(:info, "Could not create update.")
        |> redirect(to: Routes.post_path(conn, :show, update_params["news_id"]))
    end
  end
end