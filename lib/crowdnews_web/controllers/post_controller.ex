defmodule CrowdnewsWeb.PostController do
  use CrowdnewsWeb, :controller

  alias Crowdnews.News
  alias Crowdnews.News.Post
  alias Crowdnews.News.Update
  alias Phoenix.LiveView

  def index(conn, _params) do
    news_posts = News.list_news_posts()
    render(conn, "index.html", news_posts: news_posts)
  end

  def new(conn, _params) do
    changeset = News.change_post(%Post{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"post" => post_params}) do
    case News.create_post(post_params) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post created successfully.")
        |> redirect(to: Routes.post_path(conn, :show, post))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    update_changeset = News.change_update(%Update{})
    post = News.get_post!(id)
    updates = News.get_updates_for_post(post)
    Phoenix.LiveView.Controller.live_render(conn, Crowdnews.NewsLiveView, session: %{ post: post, updates: updates, update_changeset: update_changeset, conn: conn })
    #render(conn, "show.html", post: post, update_changeset: update_changeset, updates: updates)
  end

  def edit(conn, %{"id" => id}) do
    post = News.get_post!(id)
    changeset = News.change_post(post)
    render(conn, "edit.html", post: post, changeset: changeset)
  end

  def update(conn, %{"id" => id, "post" => post_params}) do
    post = News.get_post!(id)

    case News.update_post(post, post_params) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post updated successfully.")
        |> redirect(to: Routes.post_path(conn, :show, post))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", post: post, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    post = News.get_post!(id)
    {:ok, _post} = News.delete_post(post)

    conn
    |> put_flash(:info, "Post deleted successfully.")
    |> redirect(to: Routes.post_path(conn, :index))
  end
end
