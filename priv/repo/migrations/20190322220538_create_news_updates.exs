defmodule Crowdnews.Repo.Migrations.CreateNewsUpdates do
  use Ecto.Migration

  def change do
    create table(:news_updates) do
      add :news_id, :integer
      add :author, :string
      add :content, :text

      timestamps()
    end

  end
end
