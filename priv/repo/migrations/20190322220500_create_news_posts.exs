defmodule Crowdnews.Repo.Migrations.CreateNewsPosts do
  use Ecto.Migration

  def change do
    create table(:news_posts) do
      add :title, :string
      add :summary, :text

      timestamps()
    end

  end
end
